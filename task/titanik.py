import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    titles = ["Mr.", "Mrs.", "Miss."]
    median_values = []

    for title in titles:
        age = df.loc[df['Name'].str.contains(title, regex=False)]['Age'].median()
        age = round(age)
        missing_values = df.loc[(df['Name'].str.contains( title, regex=False )) & (df['Age'].isnull())].shape[0]
        median_values.append((title, missing_values, age))
    
    return median_values
